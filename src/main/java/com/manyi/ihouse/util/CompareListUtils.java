/**
 * 
 */
package com.manyi.ihouse.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.manyi.ihouse.model.CompareMo;
import difflib.DiffRow;
import difflib.DiffRowGenerator;

/**
 * <p>文件比较</p>
 * @author 何克最
 * @version $Id: CompareListUtils.java, v 0.1 2016年10月20日 上午10:53:02 Administrator Exp $
 */
public class CompareListUtils {
    public static final String Result_EQUAL = "EQUAL";  
    public static final String Result_INSERT = "INSERT";  
    public static final String Result_DELETE = "DELETE";  
    public static final String Result_CHANGE = "CHANGE";  
    /*** 
     *  
     * @param filename 
     * @return 
     */  
    public static List<String> fileToLines(String filename) {  
        List<String> lines = new LinkedList<String>();  
        String line = "";  
        try {  
            BufferedReader in = new BufferedReader(new FileReader(filename));  
            while ((line = in.readLine()) != null) {  
                lines.add(line);  
            }
            in.close();
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return lines;  
    }
    
    public static void linesToFile(String filename, String line) {
        if(StringUtils.isBlank(line)) return;
        try {  
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));  
            writer.write(line);
            writer.flush();
            writer.close();
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return;  
    }
    
    public static void linesToFile(File file, String line) {
        if(StringUtils.isBlank(line)) return;
        try {  
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(line);
            writer.flush();
            writer.close();
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return;  
    }
      
    /*** 
     * 对文件进行比较 
     * INSERT, DELETE, CHANGE, EQUAL 
     * @param fromFileName 原来的文件名 
     * @param toFileName 新的文件名 
     */  
    public static List<CompareMo> compareAll(String fromFileName, String toFileName) {  
        List<String> original = fileToLines(fromFileName);  
        List<String> revised = fileToLines(toFileName);
        return compareAll(original, revised);
    }
    
    public static List<CompareMo> compareAll(List<String> original, List<String> revised) {  
        final DiffRowGenerator.Builder builder = new DiffRowGenerator.Builder();  
        final DiffRowGenerator dfg = builder.build();  
        final List<DiffRow> rows = dfg.generateDiffRows(original, revised);  
        List<CompareMo> listCompareMo = new ArrayList<CompareMo>();  
        int i=1;  
        int oldSize = original.size();  
        int newSize = revised.size();  
        int insertSize = 0;  
        int deleteSize = 0;  
        for (final DiffRow diffRow : rows) {  
            String tag = diffRow.getTag().toString();  
            String oldLine = diffRow.getOldLine();  
            String newLine = diffRow.getNewLine();  
            if(Result_CHANGE.equals(tag)){  
                boolean isInset = false;  
                if ((i-insertSize) <= oldSize) {  
                    if(oldLine!=null&& oldLine.trim().length()==0){  
                        if(!original.get(i-1-insertSize).equals(oldLine)){
                            tag = Result_INSERT;
                            isInset = true;
                            insertSize ++;
                        }  
                    }  
                }  
                if (!isInset) {  
                    if ((i-deleteSize) <= newSize) {  
                        if(newLine!=null&& newLine.trim().length()==0){  
                            if(!revised.get(i-1-deleteSize).equals(oldLine)){  
                                tag = Result_DELETE;  
                                isInset = true;  
                                deleteSize ++;  
                            }  
                        }  
                    }  
                }  
            }  
            listCompareMo.add(new CompareMo(  
                    i, oldLine,   
                    newLine,   
                    tag  
            ));  
            i++;  
        }
        return listCompareMo;  
    }
      
    public static void main(String[] args) {  
        List<CompareMo> compares = CompareListUtils.compareAll(  
                "D:/work/WiFiPages/originalFile.txt",
                "D:/work/WiFiPages/newFile.txt");  
        for (CompareMo compare : compares) {  
            System.out.println(compare.getId() + " 结果 :" + compare.getType()  
                    + ":" + compare.getOldText() + "<>" + compare.getNewText());  
        }
    }
}
