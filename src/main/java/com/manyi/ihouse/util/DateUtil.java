package com.manyi.ihouse.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

public class DateUtil {
	
	public static SimpleDateFormat dFyMdChina = new SimpleDateFormat("yyyy年MM月dd日");
	public static SimpleDateFormat dFyMd = new SimpleDateFormat("yyyyMMdd");
	public static SimpleDateFormat dFy_M_d = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat dFy_M_dLC = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
	public static SimpleDateFormat dFHHmmLC = new SimpleDateFormat("HH:mm", Locale.CHINA);
	public static SimpleDateFormat dFMonthLC = new SimpleDateFormat("M", Locale.CHINA);
	public static SimpleDateFormat dFy_M_d_HH = new SimpleDateFormat("yyyy-MM-dd HH点", Locale.CHINA);
	public static SimpleDateFormat dFy_MMDD = new SimpleDateFormat("MM月dd日", Locale.CHINA);
	public static SimpleDateFormat dFy_MM_DD = new SimpleDateFormat("MM/dd", Locale.CHINA);
	public static SimpleDateFormat dFymdhms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	
	public static Date StringToDate(String time){
	   try {
           
           
	       return dFy_M_d.parse(time);
    } catch (ParseException e) {
        e.printStackTrace();
    }
	       return null;
	}
	
	/**
	 * 发布时间格式： 1小时内:XX分钟前 24小时内：XX小时前 7天内：X天前 超过7天：显示日期
	 * 
	 * @param date
	 * @return
	 */
	public static String fattrDate(Date date) {
		long time = System.currentTimeMillis() - date.getTime();// 得到 当前时间的毫秒数
		double hours = time / 1000 / 60 / 60d;// 小时
		String fattrStr = "";
		if ((hours / 24) >= 7) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
			fattrStr = sdf.format(date);
		} else if (hours < 1) {
			double t = (time / 1000 / 60d);
			fattrStr = (t <= 1 ? "1" : Math.round(t)) + "分钟前发布";
		} else if (1 <= hours && hours < 24) {
			fattrStr = Math.round(hours) + "小时前发布";
		} else if ((hours / 24) >= 1 && (hours / 24) < 7) {
			double t = (hours / 24d);
			fattrStr = (t <= 1 ? "1" : Math.round(t)) + "天前发布";
		}
		return fattrStr;
	}
	
	public static String formatChange(String dateStr) {
		if (dateStr == null) {
			return "";
		}
		if (dateStr.indexOf("/") != -1) {
			return dateStr.replace("/", "-");
		} else {
			return dateStr;
		}
	}
	
	/**
	 * 时间格式： X天前 超过3天：显示日期：x年x月x日
	 * 
	 * @param date
	 * @return
	 */
	public static String fattrDate2(Date date) {
		if(date == null) {
			return "";
		}
		String fattrStr = dFyMd.format(date);
		String today = dFyMd.format(new Date());
		int day = Integer.parseInt(today) - Integer.parseInt(fattrStr);
		if (day == 0) {
			fattrStr = "今天";
		} else if (day > 0 && day < 3) {
			fattrStr = day + "天前";			
		} else {
			fattrStr = dFy_M_d.format(date);
		}
		
		return fattrStr;
	}
	
	/**
	 * 时间格式： X天前 超过3天：显示日期：x年x月x日
	 * 
	 * @param date
	 * @return
	 */
	public static String fattrDate2(long dateTime) {
		if (dateTime <= 0) {
			return "-";
		}
		Calendar calendar = Calendar.getInstance();
		String today = dFyMd.format(calendar.getTime());
		calendar.setTimeInMillis(dateTime);
		String fattrStr = dFyMd.format(calendar.getTime());
		int day = Integer.parseInt(today) - Integer.parseInt(fattrStr);
		if (day == 0) {
			fattrStr = "今天";
		} else if (day > 0 && day < 3) {
			fattrStr = day + "天前";			
		} else {
			fattrStr = dFy_M_d.format(calendar.getTime());
		}
		
		return fattrStr;
	}
	
	/**
	 * 时间格式： x年x月x日
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDate(long dateTime) {
		if (dateTime <= 0) {
			return "-";
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(dateTime);
		return dFy_M_d.format(calendar.getTime());
	}

	public static String formatDate2(Object obj) {
		if (obj == null) {
			return "";
		}
		Date date = new Date();
		try {
			date = (Date) obj;
		} catch (Exception e) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(date);
	}

	/**
	 * 发布时间描述 当日：今日发布 ； 其他： XX天前发布
	 * 
	 * @param smdate
	 * @return
	 */
	public static String pubDateFormat(Date smdate) {
		Date bdate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			smdate = sdf.parse(sdf.format(smdate));
			bdate = sdf.parse(sdf.format(bdate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		String formatStr = "";

		if (between_days == 0) {
			formatStr = "今日发布";
		} else {
			formatStr = between_days + "天前发布";
		}

		return formatStr;
	}

	/**
	 * 
	 * 功能描述:将制定格式的字符串转为Date类型
	 * 
	 * <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     hubin:   2014年6月17日      新建
	 * </pre>
	 * 
	 * @param dateString
	 * @param formatStr
	 * @return
	 */
	public static Date string2date(String dateString, String formatStr) {
		Date formateDate = null;
		DateFormat format = new SimpleDateFormat(formatStr);
		try {
			formateDate = format.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return formateDate;
	}

	public static String date2string(Date date, String formatStr) {
		if(date==null){
			return "";
		}
		String strDate = "";
		Locale locale = Locale.CHINA;
		SimpleDateFormat sdf = new SimpleDateFormat(formatStr,locale);
//		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		strDate = sdf.format(date);
		return strDate;
	}
	
	public static String date2stringYmdhms(Date date) {
		if(date==null){
			return "";
		}
		String strDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		strDate = sdf.format(date);
		return strDate;
	}
	
	public static String date2stringYmdhm(Date date) {
		if(date==null){
			return "";
		}
		String strDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		strDate = sdf.format(date);
		return strDate;
	}
	
	public static Date string2date(String dateString) {
		if (StringUtils.isBlank(dateString)) {
			return null;
		}
		Date formateDate = null;
		try {
			formateDate = dFy_M_dLC.parse(dateString);
		} catch (Exception e) {
			return null;
		}
		return formateDate;
	}
	
	public static String date2string(Date date) {
		if(date==null){
			return "";
		}
		String strDate = "";
		try {
			strDate = dFy_M_dLC.format(date);
		} catch (Exception e) {
		}
		return strDate;
	}
	
	public static String date2HHmm(Date date) {
		if(date==null){
			return "";
		}
		String strDate = "";
		
		try {
			strDate = dFHHmmLC.format(date);
		} catch (Exception e) {
		}
		
		return strDate;
	}
	
	public static String date2Month(Date date) {
		if(date==null){
			return "";
		}
		String strDate = "";
		
		try {
			strDate = dFMonthLC.format(date);
		} catch (Exception e) {
		}
		
		return strDate;
	}

	public static String todayString() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");// 设置日期格式
		return df.format(new Date());// new Date()为获取当前系统时间
	}

	/**
	 * 
	 * 功能描述:格式化日期字符串显示格式
	 * 
	 * <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     hubin:   2014年6月17日      新建
	 * </pre>
	 * 
	 * @param sdate
	 *            原始日期格式 s - 表示 "yyyy-mm-dd" 形式的日期的 String 对象
	 * @param format
	 *            格式化后日期格式
	 * @return 格式化后的日期显示
	 */
	public static String dateFormat(String sdate, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Date date = string2date(sdate, "yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);

		return dateString;
	}


	/**
	 *
	 * 功能描述:格式化日期字符串显示格式
	 *
	 * <pre>
	 * Modify Reason:(修改原因,不需覆盖，直接追加.)
	 *     hubin:   2014年6月17日      新建
	 * </pre>
	 *
	 * @param sdate
	 *            原始日期格式 s - 表示 "yyyy-mm-dd" 形式的日期的 String 对象
	 * @param fromFormat 格式化前的日期格式
	 * @param toFormat
	 *            格式化后日期格式
	 * @return 格式化后的日期显示
	 */
	public static String dateStrFormat(String sdate, String fromFormat, String toFormat) {
		if(sdate==null){
			return  "";
		}
		String dateString = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(toFormat);
			Date date = string2date(sdate, fromFormat);
			dateString = formatter.format(date);
		} catch (Exception e) {
			return sdate;
		}

		return dateString;
	}
	
	/**
	 * oDate大则为正数
	 * @param fDate
	 * @param oDate
	 * @return
	 */
	public static int daysOfTwoDate(Date fDate, Date oDate) {
		if (fDate == null || oDate == null) {
			return 0;
		}
		Calendar c1 = Calendar.getInstance();
		c1.setTime(fDate);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(oDate);
		return getDaysBetween(c1, c2);
	}
	/**
	 * oDate大则为正数
	 * @param fDate
	 * @param oDate
	 * @return
	 */
	public static int daysOfTwoDate(long fDate, long oDate) {
		Calendar c1 = Calendar.getInstance();
		c1.setTimeInMillis(fDate);
		Calendar c2 = Calendar.getInstance();
		c2.setTimeInMillis(oDate);
		return getDaysBetween(c1, c2);
	}
	
	public static int getDaysBetween(Calendar d1, Calendar d2)
	{
		int change = 1;
		if (d1.after(d2)){ // swap dates so that d1 is start and d2 is end
			Calendar swap = d1;
			d1 = d2;
			d2 = swap;
			change = -1;
		}
		int days = d2.get(Calendar.DAY_OF_YEAR) - d1.get(Calendar.DAY_OF_YEAR);
		int y2 = d2.get(Calendar.YEAR);
		if (d1.get(Calendar.YEAR) != y2) {
			d1 = (Calendar) d1.clone();
			do {
				days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);
				d1.add(Calendar.YEAR, 1);
			} while (d1.get(Calendar.YEAR) != y2);
		}       
		return days*change;
	}

	/**
	 * 根据指定的年、月、日返回当前是星期几。1表示星期天、2表示星期一、7表示星期六。
	 * 
	 * @param year
	 * @param month
	 *            month是从1开始的12结束
	 * @param day
	 * @return 返回一个代表当期日期是星期几的数字。1表示星期天、2表示星期一、7表示星期六。
	 */
	public static int getDayOfWeek(String year, String month, String day) {
		Calendar cal = new GregorianCalendar(new Integer(year).intValue(), new Integer(month).intValue() - 1, new Integer(day).intValue());
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 根据指定的年、月、日返回当前是星期几。1表示星期天、2表示星期一、7表示星期六。
	 * 
	 * @param date
	 *            "yyyy/MM/dd",或者"yyyy-MM-dd"
	 * @return 返回一个代表当期日期是星期几的数字。1表示星期天、2表示星期一、7表示星期六。
	 */
	public static int getDayOfWeek(String date) {
		String[] temp = null;
		if (date.indexOf("/") > 0) {
			temp = date.split("/");
		}
		if (date.indexOf("-") > 0) {
			temp = date.split("-");
		}
		return getDayOfWeek(temp[0], temp[1], temp[2]);
	}

	/**
	 * 根据指定的年、月、日返回当前是星期几。1表示星期天、2表示星期一、7表示星期六。
	 * 
	 * @param date
	 * 
	 * @return 返回一个代表当期日期是星期几的数字。1表示星期天、2表示星期一、7表示星期六。
	 */
	public static int getDayOfWeek(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 返回当前日期是星期几。例如：星期日、星期一、星期六等等。
	 * 
	 * @param date
	 *            格式为 yyyy/MM/dd 或者 yyyy-MM-dd
	 * @return 返回当前日期是星期几
	 */
	public static String getChinaDayOfWeek(Date date) {
		String[] weeks = new String[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		int week = getDayOfWeek(date);
		return weeks[week - 1];
	}

	/**
	 * 返回当前日期是星期几。例如：星期日、星期一、星期六等等。
	 * 
	 * @param date
	 *            格式为 yyyy/MM/dd 或者 yyyy-MM-dd
	 * @return 返回当前日期是星期几
	 */
	public static String getChinaDayOfWeek(String date) {
		String[] weeks = new String[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		int week = getDayOfWeek(date);
		return weeks[week - 1];
	}

	/**
	 * 返回当前日期是周几。例如：周日、周一、周六等等。
	 * 
	 * @param date
	 *            格式为 yyyy/MM/dd 或者 yyyy-MM-dd
	 * @return 返回当前日期是星期几
	 */
	public static String getChinaDayOfWeekZhou(Date date) {
		String[] weeks = new String[] { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
		int week = getDayOfWeek(date);
		return weeks[week - 1];
	}

	/**
	 * 今天0点时间搓
	 * 
	 * @return
	 */
	public static long getTodayTimeInMillis() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
	
    /**
     * 
     * 功能描述:将US格式的日期格式化为指定日期格式
     *
     * <pre>
     *      eg.from "12/Aug/2014:23:57:47" to "2014-08-12 23:57:47"
     * Modify Reason:(修改原因,不需覆盖，直接追加.)
     *     hu-bin:   2014年8月14日      新建
     * </pre>
     *
     * @param datestr 日期字符串
     * @param oraFormat 原来格式
     * @param desFormat 转化后的格式
     * @return
     */
    public static String USdatestrFormat(String datestr, String oraFormat, String desFormat){
        SimpleDateFormat bartDateFormat = new SimpleDateFormat(oraFormat, java.util.Locale.US);
        Date d = new Date();
        try {
            d = bartDateFormat.parse(datestr);
        }
        catch (ParseException e) {// TODO 自动生成 catch 块                e.printStackTrace();
        }
        String dstr = DateUtil.date2string(d,desFormat);
        return dstr;
    }


    /**
     * 取得给定格式的昨天的日期输出
     *
     * @param format
     *            日期输出的格式
     * @return String 给定格式的日期字符串.
     */
    public static String getFormatYestoday(String format) {
        return getFormatCurrentAdd(-1, format);
    }

    /**
     * 获得当前日期固定间隔天数的日期，如前60天dateAdd(-60)
     *
     * @param amount
     *            距今天的间隔日期长度，向前为负，向后为正
     * @param format
     *            输出日期的格式.
     * @return java.lang.String 按照格式输出的间隔的日期字符串.
     */
    public static String getFormatCurrentAdd(int amount, String format) {

        Date d = getDateAdd(new Date(), amount);

        return getFormatDateTime(d, format);
    }

    /**
     * 根据给定的格式与时间(Date类型的)，返回时间字符串。最为通用。
     * 为空时范围空字符串
     * @param date
     *            指定的日期
     * @param format
     *            日期格式字符串
     * @return String 指定格式的日期字符串.
     */
    public static String getFormatDateTime(Date date, String format) {
		if(date==null){
			return "";
		}
		String str = "";
		try {
		    SimpleDateFormat sdf = new SimpleDateFormat(format);
		    str = sdf.format(date);
		} catch(Exception e) {
		    
		}
        
        return str;
    }

    /**
     * 
     * 功能描述:取得给定日期加上一定天数后的日期对象.
     *
     * <pre>
     * Modify Reason:(修改原因,不需覆盖，直接追加.)
     *     hu-bin:   2014年8月25日      新建
     * </pre>
     *
     * @param date
     *            给定的日期对象
     * @param amount
     *            需要添加的天数，如果是向前的天数，使用负数就可以.
     * @return Date 加上一定天数以后的Date对象.
     */
    public static Date getDateAdd(Date date, int amount) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, amount);
        return cal.getTime();
    }
    
    public static String formatDate(Date date) {
    	return dFyMdChina.format(date);
    }
    
    public static String formatDateYYMMDD(Date date){
    	String dateStr = "";
    	try {
			dateStr = dFy_M_d.format(date);
		} catch (Exception e) {
			
		}
    	return dateStr;
    	
    }
    
    /**
     * 获取日期零点
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月28日 下午1:48:13
     */
    public static Date getTimeZero(Date date){
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(date);
		 cal.set(Calendar.HOUR_OF_DAY, 0);
		 cal.set(Calendar.MINUTE, 0);
		 cal.set(Calendar.SECOND, 0);
		 cal.set(Calendar.MILLISECOND, 0);
		 cal.add(Calendar.DAY_OF_MONTH, 0);
		 return  cal.getTime();
    }
    
    /**
     * date2string XXXX年XX月XX日
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月29日 下午2:15:40
     */
    public static String date2StringChina(Date date) {
    	if(date==null){
			return "";
		}
		String strDate = "";
		try {
			strDate = dFyMdChina.format(date);
		} catch (Exception e) {
		}
		return strDate;
    }
    
    /**
     * 获取日期后多少天  负数往前
     * @since 1.0 
     * @param before
     * @param days
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月29日 下午3:54:03
     */
    public static Date addDate(Date before,int days){
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(before);
    	calendar.add(Calendar.DATE, days);
    	return calendar.getTime();
    }
    
    /**
     * 判断当天是否在两个日期间
     * @since 1.0 
     * @param begin
     * @param after
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月29日 下午5:31:32
     */
    public static boolean isInBetween(Date begin,Date after){
    	Date date = new Date();
    	if (!date.before(begin) && !date.after(after)) {
			return true;
		}
    	return false;
    }
    
    /**
     * date2string XXXX年XX月XX日
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月29日 下午2:15:40
     */
    public static String date2YYYYMMDDHH(Date date) {
    	if(date==null){
			return "";
		}
		String strDate = "";
		try {
			strDate = dFy_M_d_HH.format(date);
		} catch (Exception e) {
		}
		return strDate;
    }
    
    /**
     * date2string XX月XX日
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月29日 下午2:15:40
     */
    public static String date2MMDD(Date date) {
    	if(date==null){
			return "";
		}
		String strDate = "";
		try {
			strDate = dFy_MMDD.format(date);
		} catch (Exception e) {
		}
		return strDate;
    }
    
    /**
     * date2string 08/01
     * @since 1.0 
     * @param date
     * @return   08/01
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年8月12日 下午6:34:36
     */
    public static String date2MM_DD(Date date) {
    	if(date==null){
			return "";
		}
		String strDate = "";
		try {
			strDate = dFy_MM_DD.format(date);
		} catch (Exception e) {
		}
		return strDate;
    }
    
    /**
     * 判断日期是否在当天
     * @since 1.0 
     * @param time
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年8月30日 下午12:35:22
     */
    public static boolean inDay(Long time){
    	if (time != null) {
    		//当天零点
    		long begin = getTimeZero(new Date()).getTime();
    		//次日零点
    		long end = getTimeZero(addDate(new Date(), 1)).getTime();
    		if (time >= begin && time < end) {
				return true;
			}
		}
    	return false;
    }
    
    /**
     * 获取二手房发布时间 YYYY-mm-dd
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年8月31日 下午3:58:08
     */
    public static String getPubDate(Date date){
    	try {
			if (date != null) {
				return dFy_M_dLC.format(date);
			}
		} catch (Exception e) {
		}
    	return "-";
    }

	/**
     * yyyy-MM-dd HH:mm:ss
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年7月29日 下午2:15:40
     */
    public static String date2YMDHMS(Date date) {
    	if(date==null){
			return "";
		}
		String strDate = null;
		try {
			strDate = dFymdhms.format(date);
		} catch (Exception e) {
		}
		return strDate;
    }
    
    /**
     * long  ---》date
     * @since 1.0 
     * @param date
     * @return
     * <br><b>作者： @author huangjun</b>
     * <br>创建时间：2016年9月22日 下午1:33:52
     */
    public static Date long2Date(Long date){
        if (date == null || date <= 0) {
            return null;
        }
        return new Date(date);
    }
    
    public static Date String2Date(String time,String format){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.parse(time);
     } catch (ParseException e) {
         e.printStackTrace();
     }
            return null;
     }
    
}