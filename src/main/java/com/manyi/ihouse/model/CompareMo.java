/**
 * 
 */
package com.manyi.ihouse.model;

import lombok.Data;

/**
 * <p>注释</p>
 * @author 何克最
 * @version $Id: CompareMo.java, v 0.1 2016年10月20日 上午10:56:10 Administrator Exp $
 */
@Data
public class CompareMo {
    public CompareMo(int id, String oldText, String newText, String type) {
        this.id = id;
        this.type = type;
        this.oldText = oldText;
        this.newText = newText;
    }
    private int id;
    private String type;
    private String oldText;
    private String newText;
}
