
export COUNTRY_HOME=/usr/share/software
export HOMEDIR=$COUNTRY_HOME/userappBatchSever
export JAVA_HOME=$COUNTRY_HOME/jdk1.7.0_55
export M2_HOME=$COUNTRY_HOME/apache-maven-3.3.9

export PATH=${M2_HOME}/bin:$PATH
export classpath=$JAVA_HOME/rt.jar:$JAVA_HOME/tools.jar
export GIT_SRC_HOME=/usr/share/software/git/userappBatchSever
cd $GIT_SRC_HOME
git pull origin master
rm -rf $GIT_SRC_HOME/target
mvn clean package -Dmaven.test.skip=true
rm -rf $HOMEDIR/lib/*
cp $GIT_SRC_HOME/target/userappBatchSever-0.0.1-SNAPSHOT.jar $HOMEDIR/lib/userappBatchSever.jar
cp $GIT_SRC_HOME/target/lib/* $HOMEDIR/lib/

