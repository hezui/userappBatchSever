#!/bin/bash

export HOMEDIR=$(cd "$(dirname "$0")"; pwd)/..
export JAVA_HOME=/usr/share/software/jdk1.7.0_55
export JAVA_CMD=$JAVA_HOME/bin/java

CLASSPATH=$HOMEDIR/etc/
for file in $HOMEDIR/lib/*
do
        CLASSPATH=$CLASSPATH:$file
done

export JAVA_LIB_MAINJAVA=com.manyi.ihouse.schedue.GetLianjiaWebInfo

appName=$1
logPath="$HOMEDIR/logs/"$appName"_"`date +%Y%m%d%H%M%S`"_log.log"
pid=`ps aux| grep "appName="$appName | grep -v grep | sed -n  '1P' | awk '{print $2}'`
if [ -z $pid ]; then
        echo "no inv process"
        $JAVA_CMD -Xms1g -Xmx1g -DappName=$appName -classpath "$CLASSPATH" $JAVA_LIB_MAINJAVA $2 >$logPath 2>&1 &
        #echo "$JAVA_CMD -Xms1g -Xmx1g -DappName=$appName -classpath $CLASSPATH $JAVA_LIB_MAINJAVA $2 >$logPath"
else
        kill -9 $pid
        echo "find inv process killed" $pid 
        $JAVA_CMD -Xms1g -Xmx1g -DappName=$appName -classpath "$CLASSPATH" $JAVA_LIB_MAINJAVA $2 >$logPath 2>&1 &
fi
